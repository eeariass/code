#!/usr/bin/env perl
use strict;
use warnings;

my $word = "anan";

if (reverse($word) eq $word) {
    print "$word is palindrome";
} else {
    print "$word is not palindrome";
}
